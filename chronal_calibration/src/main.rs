use chronal_calibration::*;
use std::collections::HashSet;

fn main() {
    let freq = parse_input("data/input.txt");
    println!("Part 1: {}", freq.iter().sum::<i32>());

    let mut freq = freq.iter().cycle();
    let mut seen = HashSet::new();
    let mut cumsum: i32 = 0;

    while let Some(val) = freq.next() {
        cumsum += val;
        if !seen.insert(cumsum) {
            println!("Part 2: {}", cumsum);
            break;
        }
    }
}

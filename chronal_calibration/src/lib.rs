use std::fs;

pub fn parse_input(filename: &str) -> Vec<i32> {
    let file = fs::read_to_string(filename).unwrap();
    file.lines().filter_map(|l| l.parse::<i32>().ok()).collect()
}
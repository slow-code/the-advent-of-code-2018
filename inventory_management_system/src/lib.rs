use std::collections::HashMap;

pub struct BoxId {
    pub id: String,
    nchar: HashMap<char, usize>,
}

impl BoxId {
    pub fn new(id: String) -> Self {
        let mut nchar = HashMap::new();
        for ch in id.chars() {
            let count = nchar.entry(ch).or_insert(0);
            *count += 1;
        }
        Self { id, nchar }
    }

    pub fn repeats(&self, rep: &usize) -> usize {
        self.nchar.values().any(|x| x == rep) as usize
    }
}

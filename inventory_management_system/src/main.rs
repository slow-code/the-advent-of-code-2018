use inventory_management_system::BoxId;
use itertools::Itertools;
use std::fs;

fn main() {
    let puzzle: String = fs::read_to_string("data/input.txt").unwrap();
    
    let boxes: Vec<BoxId> = puzzle.lines().map(|l| BoxId::new(l.to_string())).collect();

    let checksum: usize = [2, 3]
        .iter()
        .map(|n| boxes.iter().map(|b| b.repeats(n)).sum())
        .fold(1, |acc, x: usize| acc * x);

    println!("Part 1: {}", checksum);

    let common_id = boxes
        .iter()
        .tuple_combinations()
        .map(|(x, y)| {
            x.id.chars()
                .zip(y.id.chars())
                .filter_map(|(x, y)| if x == y { Some(x) } else { None })
                .collect::<String>()
        })
        .max_by_key(|x| x.len())
        .unwrap();

    println!("Part 2: {}", common_id);
}

use crate::log_parser::{extract_guard_id, parse_sleep_ranges};
use std::collections::HashMap;
use std::ops::Range;

trait Duration {
    fn duration(&self) -> u32;
}

impl Duration for Range<u32> {
    fn duration(&self) -> u32 {
        self.end - self.start
    }
}

#[derive(Debug)]
pub struct Guard {
    pub guard_id: u32,
    pub sleep_ranges: Vec<Range<u32>>,
}

impl Guard {
    pub fn new(log: Vec<&str>) -> Self {
        let mut log = log.into_iter().rev();
        let head = log.next().unwrap();
        let guard_id = extract_guard_id(head);
        let sleep_ranges = parse_sleep_ranges(log);
        Self {
            guard_id,
            sleep_ranges,
        }
    }

    pub fn total_sleep(&self) -> u32 {
        self.sleep_ranges.iter().map(|x| x.duration()).sum()
    }

    pub fn mode(&self) -> (u32, u32) {
        let mut counter: HashMap<_, _> = HashMap::new();

        for sleep in &self.sleep_ranges {
            for minute in sleep.clone() {
                counter
                    .entry(minute)
                    .and_modify(|count| *count += 1)
                    .or_insert(1_u32);
            }
        }

        counter.into_iter().max_by_key(|(_, v)| *v).unwrap()
    }
}

#[derive(Debug)]
pub struct Guards {
    pub guards: Vec<Guard>,
}

impl Guards {
    pub fn new(shifts: Vec<Vec<&str>>) -> Self {
        shifts
            .into_iter()
            .map(|log| Guard::new(log))
            .collect::<Guards>()
    }

    pub fn group_by_id(self) -> Self {
        let mut staging = HashMap::<u32, Vec<_>>::new();

        self.guards.into_iter().for_each(|guard| {
            staging
                .entry(guard.guard_id)
                .and_modify(|v| v.extend(guard.sleep_ranges))
                .or_insert(Vec::new());
        });

        staging
            .into_iter()
            .filter(|(_, v)| !v.is_empty())
            .map(|(k, v)| Guard {
                guard_id: k,
                sleep_ranges: v,
            })
            .collect::<Guards>()
    }
}

impl FromIterator<Guard> for Guards {
    fn from_iter<T: IntoIterator<Item = Guard>>(iter: T) -> Self {
        let guards = iter.into_iter().collect::<Vec<_>>();
        Guards { guards }
    }
}

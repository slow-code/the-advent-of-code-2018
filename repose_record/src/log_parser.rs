use chrono::{NaiveDateTime, Timelike};
use itertools::Itertools;
use std::cmp::Reverse;
use std::ops::Range;

const TIMESTAMP_FORMAT: &str = "[%Y-%m-%d %H:%M";

pub fn parse_log<'a>(file: &String) -> Vec<Vec<&str>> {
    let mut shifts = file.lines().collect::<Vec<_>>();
    shifts.sort_by_key(|&x| Reverse(x));
    shifts
        .split_inclusive(|&x| x.contains("Guard"))
        .map(|x| x.to_owned())
        .collect()
}

fn extract_minute(log: &str, fmt: &str) -> u32 {
    let (dt, _) = log.split_once("]").unwrap();
    let dt = NaiveDateTime::parse_from_str(dt, fmt).unwrap();
    dt.time().minute()
}

pub fn extract_guard_id(log: &str) -> u32 {
    let (_, guard_id) = log.split_at(25);
    guard_id
        .matches(char::is_numeric)
        .collect::<Vec<_>>()
        .join("")
        .parse::<u32>()
        .unwrap()
}

pub fn parse_sleep_ranges<'a>(log: impl Iterator<Item = &'a str>) -> Vec<Range<u32>> {
    log.map(|x| extract_minute(x, TIMESTAMP_FORMAT))
        .tuples()
        .map(|(start, end)| Range { start, end })
        .collect::<Vec<Range<_>>>()
}

mod guard;
mod log_parser;
mod summary;

use guard::Guards;
use std::cmp::Reverse;
use std::fs;
use summary::Summaries;

fn main() {
    let file = fs::read_to_string("../data/input.txt").unwrap();
    let shifts = parse_log(&file);
    let guards = Guards::new(shifts);
    let guards = guards.group_by_id();
    let stats = Summaries::from(guards);

    println!("Part 1: {:#?}", stats.strategy_1());
    println!("Part 2: {:#?}", stats.strategy_2());
}

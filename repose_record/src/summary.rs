use crate::guard::{Guard, Guards};

pub struct Summary {
    pub guard_id: u32,
    pub total_sleep: u32,
    pub minute: u32,
    pub count: u32,
}

impl Summary {
    pub fn new(guard: &Guard) -> Self {
        let (minute, count) = guard.mode();
        Summary {
            guard_id: guard.guard_id,
            total_sleep: guard.total_sleep(),
            minute,
            count,
        }
    }

    pub fn result(&self) -> u32 {
        self.guard_id * self.minute
    }
}

impl From<Guards> for Summaries {
    fn from(guards: Guards) -> Self {
        let stats = guards
            .guards
            .iter()
            .map(|x| Summary::new(x))
            .collect::<Vec<_>>();
        Self { stats }
    }
}

pub struct Summaries {
    pub stats: Vec<Summary>,
}

impl Summaries {
    pub fn strategy_1(&self) -> u32 {
        self.stats
            .iter()
            .max_by_key(|x| x.total_sleep)
            .unwrap()
            .result()
    }

    pub fn strategy_2(&self) -> u32 {
        self.stats.iter().max_by_key(|x| x.count).unwrap().result()
    }
}

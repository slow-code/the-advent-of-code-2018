use fabric_squares::*;
use std::collections::HashMap;

fn main() {
    let claims = parse_input("data/input.txt");

    let mut claimed_squares: HashMap<(usize, usize), usize> = HashMap::new();

    claims
        .iter()
        .flat_map(|x| x.coords.to_owned())
        .for_each(|x| {
            claimed_squares
                .entry(x)
                .and_modify(|counter| *counter += 1)
                .or_insert(1);
        });

    println!(
        "Part 1: {}",
        claimed_squares.values().filter(|&x| *x > 1).count()
    );

    let intact = claims
        .iter()
        .find(|x| {
            x.coords
                .iter()
                .filter_map(|i| claimed_squares.get(i))
                .all(|&x| x == 1)
        })
        .unwrap();

    println!("Part 2: {}", intact.id);
}

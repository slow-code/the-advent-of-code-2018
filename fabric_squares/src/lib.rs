use regex::Regex;
use std::fs;

pub fn parse_input(path: &str) -> Vec<FabricClaim> {
    let file = fs::read_to_string(path).unwrap();
    let pattern: &str = r"#(?P<id>\d+) @ (?P<x>\d+),(?P<y>\d+): (?P<dx>\d+)x(?P<dy>\d+)";
    let re = Regex::new(pattern).unwrap();
    file.lines()
        .map(|claim| FabricClaim::new(claim, &re))
        .collect()
}

pub struct FabricClaim {
    pub id: usize,
    pub x: usize,
    pub y: usize,
    pub dx: usize,
    pub dy: usize,
    pub coords: Vec<(usize, usize)>,
}

impl FabricClaim {
    pub fn new(claim: &str, pattern: &Regex) -> Self {
        let caps = pattern.captures(claim).unwrap();
        let id = caps["id"].parse().unwrap();
        let x = caps["x"].parse().unwrap();
        let y = caps["y"].parse().unwrap();
        let dx = caps["dx"].parse().unwrap();
        let dy = caps["dy"].parse().unwrap();
        let coords = FabricClaim::squares(x, y, dx, dy);
        
        Self {
            id,
            x,
            y,
            dx,
            dy,
            coords,
        }
    }

    fn squares(x: usize, y: usize, dx: usize, dy: usize) -> Vec<(usize, usize)> {
        (x..x + dx)
            .flat_map(|x| (y..y + dy).map(|y| (x, y)).collect::<Vec<_>>())
            .collect()
    }
}

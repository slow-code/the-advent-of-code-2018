mod react;

use std::fs;
use react::react;

fn main() {
    let polymer = fs::read_to_string("data/input.txt").unwrap();
    let polymer = polymer.trim();

    let part_one = react(polymer, None);

    println!("Part 1: {:?}", part_one.len());

    let part_two = ('a'..='z')
        .into_iter()
        .map(|c| react(polymer, Some(c)))
        .min_by_key(|x| x.len())
        .unwrap();

    println!("Part 2: {:?}", part_two.len());
}

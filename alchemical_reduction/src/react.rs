use std::collections::VecDeque;

pub fn react(polymer: &str, remove: Option<char>) -> String {
    let units = polymer
        .chars()
        .filter_map(|c| {
            if let Some(rm) = remove {
                if rm == c.to_ascii_lowercase() {
                    None
                } else {
                    Some(c as u8)
                }
            } else {
                Some(c as u8)
            }
        })
        .fold(VecDeque::<u8>::new(), |mut units, unit| {
            match units.back() {
                Some(prev) => {
                    if prev.abs_diff(unit) == 32 {
                        units.pop_back();
                    } else {
                        units.push_back(unit);
                    }
                }
                None => units.push_back(unit),
            }
            units
        });

    String::from_utf8(Vec::from(units)).unwrap()
}
